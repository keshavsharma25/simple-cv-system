# CV System

---

The features of CV System includes rotation, grayscale and can help you save the resultant video. Either of the flags are optional means if one don't want rotate as a feature in the video then he/she can just not write --rotate in the command line.

The command to run the python script is -

```
python main.py -v "stack_segmentation_video.mp4" --rotate --grayscale --save
```

The FPS gets logged into the [info.log](info.log) and if --save flag is used then the output will be saved in output folder.

Implemented ✅ -

- log the FPS
- command line arguments
- Modularity

Not Implemented ❌ -

- Multiprocessing

---

### Setup Environment

---

Copy this in your command line.

```
python -m venv .venv
./.venv/Scripts/activate
pip install requirements.txt
```
