from ast import arg
import cv2
import argparse
import os
import logging
import time


def get_args():
    arg_parse = argparse.ArgumentParser()

    arg_parse.add_argument('-v', '--video', type=str, required=True)
    arg_parse.add_argument('-r', '--rotate', action='store_true')
    arg_parse.add_argument('-g', '--grayscale', action='store_true')
    arg_parse.add_argument('-s', '--save', action='store_true')

    return arg_parse.parse_args()


def log_config():

    logging.basicConfig(filename='info.log',
                        format='%(asctime)s %(message)s',
                        datefmt='%m/%d/%Y %I:%M:%S %p',
                        level=logging.INFO)
    logger = logging.getLogger()

    return logger


def save_video(vid, rotate=True, grayscale=True, save=True):

    new_frame_time = 0
    prev_frame_time = 0

    cap = cv2.VideoCapture(vid)
    filename = os.path.basename(vid).split('.')[0]

    os.makedirs('output', exist_ok=True)

    if rotate and grayscale:
        out_file = "./output/{}_rotate_grayscale.mp4".format(filename)
    elif rotate:
        out_file = "./output/{}_rotate.mp4".format(filename)
    elif grayscale:
        out_file = "./output/{}_grayscale.mp4".format(filename)
    else:
        out_file = "./output/{}.mp4".format(filename)

    fourcc = cv2.VideoWriter_fourcc('m', 'p', '4', 'v')
    fps = cap.get(cv2.CAP_PROP_FPS)
    height = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
    width = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))

    logger.info("Video: {}".format(vid))
    logger.info("Height: {}     Width: {}".format(height, width))

    if save:
        if rotate and grayscale:
            result = cv2.VideoWriter(
                out_file, fourcc, fps, (height, width), 0)
        elif rotate:
            result = cv2.VideoWriter(
                out_file, fourcc, fps, (height, width))
        elif grayscale:
            result = cv2.VideoWriter(
                out_file, fourcc, fps, (width, height), 0)
        else:
            result = cv2.VideoWriter(
                out_file, fourcc, fps, (width, height))

    while(cap.isOpened()):
        ret, frame = cap.read()

        if ret:
            if rotate:
                frame = cv2.rotate(frame, cv2.ROTATE_90_CLOCKWISE)

            if grayscale:
                frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

            if save:
                result.write(frame)

            new_frame_time = time.time()
            curr_fps = 1 / (new_frame_time - prev_frame_time)
            prev_frame_time = new_frame_time

            logger.info("FPS: {}".format(int(curr_fps)))
            resized = cv2.resize(frame, (0, 0), fx=0.2, fy=0.2)
            cv2.putText(resized, str(int(curr_fps)), (10, 20),
                        cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 255), 2)

            cv2.imshow('frame', resized)

            if cv2.waitKey(1) & 0xFF == ord('q'):
                break
        else:
            break

    cap.release()
    if save:
        result.release()
    cv2.destroyAllWindows()


if __name__ == "__main__":

    args = get_args()

    logger = log_config()

    save_video("stack_segmentation_video.mp4",
               args.rotate, args.grayscale, args.save)
